#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/shm.h>
#include "stdio.h"
#include "stdlib.h"
#include "unistd.h"

int main() {

  pid_t pid_fils;
  int E = 0;
  int shmid;
  int * shm;
  int a;

  if ((shmid = shmget(IPC_PRIVATE, sizeof(int), IPC_CREAT | 0666)) < 0) {
    perror("shmget");
    exit(1);
  }

  if ((shm = shmat(shmid, NULL, 0)) == (int * ) - 1) {
    perror("shmat");
    exit(1);
  }

  ( * shm) = 0;

  if ((pid_fils = fork()) == -1) {
    perror("Création du fils");
    exit(1);
  }

  if (pid_fils == 0) {
    for (int i = 0; i < 100; i++) {
      a = * shm;
      usleep((rand() % (101 - 20)));
      a++;
      * shm = a;
      usleep((rand() % (101 - 20)));
      printf("fils: boucle %d  -  %d \n", i, * shm);
    }
  } else {
    for (int i = 0; i < 100; i++) {
      a = * shm;
      usleep((rand() % (101 - 20)));
      a++;
      * shm = a;
      usleep((rand() % (101 - 20)));
      printf("pere: boucle %d  -  %d \n", i, * shm);
    }
  }
}