#include <sys/types.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <sys/shm.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#include "sem_pv.h"

int main() {

    pid_t pid_fils;
    // int shmid;
    int *buf;
    int *shm;
    int a;

    init_semaphore();

    // ex_mut
    val_sem(1, 1);

    // nb de place occupé
    val_sem(2, 0); // P(1) dans correction

    // nb de place libre
    val_sem(3, 5); // P(0) dans correction
    int shmid = shmget(IPC_PRIVATE, sizeof(int) * 5, 0606);
    if (shmid < 0) {
        perror("shmget");
        exit(1);
    }

    if ((buf = (int*)shmat(shmid, NULL, 0)) == (int * ) - 1) {
        perror("shmat");
        exit(1);
    }

    if ((pid_fils = fork()) == -1){
        perror("fork()");
    }

    if (pid_fils == 0) {
        // fils
        int index_fils = 0;
        for(int i=0; i<50; i++) {
            // produire un nombre
            P(3);
            /*
            Dans l'algo de cours, il faut ce sémaphore ex_mut pour un accès mutuelle au tampon.
            Ici, dans ce cas particulier, deux processus ne peut pas écrire et lire à la fois dans la même case de tampon, 
            sinon, ça signifie soit que l'écrivain a dépassé le tampon 5
            */
            // P(1);
            printf("ecrire dans tampon %d <- %d\n",index_fils % 5, i+1);
            buf[index_fils % 5] = i+1;
            index_fils++;
            usleep(1000*10*(rand()%6));
            // V(1);
            V(2);
        }
        exit(0);
    } else {
        // pere
        int index_pere = 0;
        for(int j=0; j<50; j++) {
            P(2);
            // P(1);
            printf("lire depuis tampon %d -> %d\n",index_pere % 5, buf[index_pere % 5]);
            index_pere++;
            usleep(1000*100*(rand()%6));
            // V(1);
            V(3);
            // consommer un nombre
        }
        wait(NULL);
    }
    // détruire le mémoire partagé
    if(shmctl(shmid,IPC_RMID,NULL)==-1){
		perror("Error - shmctl");
		exit(1);
	}
    // détruire sémaphore
    detruire_semaphore();
    return 0;
}