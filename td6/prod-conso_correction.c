#include "sem_pv.h"

int main(){
	int shmid = shmget(IPC_PRIVATE,5*sizeof(int),0606);
	if(shmid == -1){
		perror("Error - shmget");
		exit(1);
	}

	int *buf = (int*) shmat(shmid,NULL,0);
	if(buf == (int*)-1){
		perror("Error - shmat");
		exit(1);
	}
	init_semaphore();
	val_sem(0,5);
	val_sem(1,0);

	pid_t child = fork();

	if(child < 0 ){
		printf("erreur avec fork");
		exit(1);
	}

	if(child == 0){
		int index_fil = 0;
		for(int i=0;i<50;i++){
			P(0);
			buf[index_fil % 5] = i+1;
			printf("Ecrire : %d\n",buf[index_fil % 5]);
			index_fil++;
			usleep(1000*100*(rand()%6));
			V(1);
		}
		exit(0);
	}else{
		int index_pere = 0;
		for(int j=0;j<50;j++){
			P(1);
			printf("Lit : %d\n",buf[index_pere % 5]);
			index_pere++;
			usleep(1000*100*(rand()%6));
			V(0);
		}
		wait(NULL);
	}
	if(shmctl(shmid,IPC_RMID,NULL)==-1){
		perror("Error - shmctl");
		exit(1);
	}
	detruire_semaphore();
	return 0;
}
