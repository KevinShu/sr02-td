// struct sembuf {
//     unsigned short sem_num;  // Numéro du sémaphore dans l'ensemble
//     short sem_op;            // Opération à effectuer sur le sémaphore
//     short sem_flg;           // Options de l'opération
// };

// int semop(int semid, struct sembuf *sops, size_t nsops);
// semid est l'identifiant de l'ensemble de sémaphores, obtenu à l'aide de la fonction semget.
// sops est un pointeur vers un tableau de structures sembuf. Chaque élément de ce tableau spécifie une opération à effectuer sur un sémaphore de l'ensemble.
// nsops est le nombre d'éléments dans le tableau sops.


#include "semaph.h"

static int semid = -1; // fonction pas encore appele
/* Static : Meme si redeclare, la valeur ne change pas */

struct sembuf op_P = {-1, -1, 0}, op_V = {-1, 1, 0};

int init_semaphore() {

    int i;

    union semun arg0;
    arg0.val = 0;

    if (semid != -1) {
        fprintf(stderr, "init_semaphore(): fonction deja appele");
        // printf("fonction deja appele");
        return -1;
    }

    // IPC_PRIVATE pour ne pas preciser une clef vu que utilisation entre parent et fils
    // dans cette bibliotheque, un semid corresponds a plusieur semaphore
    // on creer un groupe de semaphore avec semget
    // le semid pointe sur une sorte de tableau qui stocke le valeur de chaque semaphore: 0|1|2|3|4
    if ((semid = semget(IPC_PRIVATE, N_SEM, 0666)) == -1) {
        // printf("echec de creation");
        fprintf(stderr, "init_semaphore(): echec de creation");
        return -2;
    }

    for (i = 0; i < N_SEM; i++) {
        semctl(semid, i, SETVAL, arg0);
    }

    return 0;
}

int detruire_semaphore() {
    int val;
    if (semid != -1) {
        val = semctl(semid, IPC_RMID, 0);
        semid = -1;
        return val;
    } else {
        fprintf(stderr, "detruire_semaphore(): init_semaphore n'a pas ete appele avant");
        return -1;
    }
}

int val_sem(int sem, int val) {
    union semun arg0;
    arg0.val = val;
    // if (semid != -1) {
    //     if ((semid = semctl(semid, sem, SETVAL, arg0)) == -1){
    //         fprintf(stderr, "val_sem(): semaphore n'existe pas");
    //         return -2;
    //     } else {
    //         return semid;
    //     }
    // } else {
    //     fprintf(stderr, "val_sem(): init_semaphore n'a pas ete appele avant");
    //     return -1;
    // }
    if (semid == -1){
        fprintf(stderr, "val_sem(): init_semaphore n'a pas ete appele avant");
        return -1;
    }

    if (sem<0 || sem>=N_SEM) {
        fprintf(stderr, "val_sem(): semaphore n'existe pas");
        return -2;
    }

    return (semctl(semid, sem, SETVAL, arg0));
}

int P(int sem) {
    // struct sembuf arg0;
    // arg0.sem_num = sem;
    // arg0.sem_op = -1;
    // int val_retour;

    if (semid == -1) {
        fprintf(stderr, "P(): init_semaphore n'a pas ete appele avant");
        return -1;
    }

    // if ((val_retour = semop(semid, &arg0, 1)) == -1) {
    //     fprintf(stderr, "P(): semaphore n'existe pas");
    //     return -2;
    // } else {
    //     return val_retour;
    // }

    if (sem<0 || sem>=N_SEM) {
        fprintf(stderr, "P(): numero de semaphore incorrect");
        return -2;
    }

    // spécifier le numéro de sémaphore dans sembuf
    op_P.sem_num = sem;
    return (semop(semid, &op_P, 1));

}

int V(int sem) {
    // struct sembuf arg0;
    // arg0.sem_num = sem;
    // arg0.sem_op = 1;
    // int val_retour;

    if (semid == -1) {
        fprintf(stderr, "V(): init_semaphore n'a pas ete appele avant");
        return -1;
    }

    // if ((val_retour = semop(semid, &arg0, 1)) == -1) {
    //     fprintf(stderr, "V(): semaphore n'existe pas");
    //     return -2;
    // } else {
    //     return val_retour;
    // }

    if (sem<0 || sem>=N_SEM) {
        fprintf(stderr, "V(): numero de semaphore incorrect");
        return -2;
    }

    // spécifier le numéro de sémaphore dans sembuf
    op_V.sem_num = sem;
    return (semop(semid, &op_V, 1));

}