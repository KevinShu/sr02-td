#include "sharemem.h"
#define SHMSZ 27

int main(){
    char c;
    // id of process to create
    int shmid;
    key_t key = 2017;
    char *shm, *s;
    // creer le segment
    // 0666 = permisson of writing and reding on the shm segment
    if((shmid = shmget(key, SHMSZ, IPC_CREAT | 0666)) < 0){
        perror("shmget");
        exit(1);
    }
    printf("Creation d'un segment de memoire avec shmid = %d\n", shmid);
    // assign the pointer
    if((shm = shmat(shmid, NULL, 0)) == (char*)-1){
        perror("shmat");
        exit(1);
    }
    // ensure the shm is empty (effacer le contenu de la region partagee)
    memset(shm, 0, SHMSZ);
    s = shm;
    for(c = 'a'; c <= 'z'; c++){
        *s++ = c;
    }
    *s = '\0';
    while(*shm != '*'){
        // wait the client to change the first character of the shm segment
        sleep(1);
    }
    if(shmdt(shm) == -1){
        perror("shmdt");
        exit(1);
    }
    printf("le client met fin au processus\n");
    printf("le procesus est temine\n");
    if(shmctl(shmid, IPC_RMID, NULL) == 1){
        perror("shmctl");
        exit(1);
    }
    exit(0);
    return 0;
}