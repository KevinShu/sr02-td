#include "sharemem.h"
#include "copy.c"

int main(int argc, char* argv[]){
    int octets_lus;
    int child_pid;
    int fd, fd1, fd2;
    if (argc != 3){
        fprintf(stderr, "Usage: %s fichier1 fichier2", argv[0]);
        return 1;
    }
    if ((fd1 = open(argv[1], O_RDONLY)) == -1)
    {
        fprintf(stderr, "Echec d'ouverture du fichier %s\n", argv[1]);
        return 1;
    }
    if ((fd2 = open(argv[2], O_RDONLY)) == -1)
    { 
        fprintf(stderr, "Echec d'ouverture du fichier %s\n", argv[2]);
        return 1;
    }
    if((child_pid = fork()) == -1){
        // afficher un message en fonction de la valeur errno
        perror("echec de fork()");
        return 1;
    }
    if (child_pid > 0){
        /* PARENT CODE */
        fd = fd1;

    } else {
        /* CHILD CODE */
        fd = fd2;
    }
    // STDOUT_FILENO: descripteur du fichier de sortie standard
    octets_lus = copiefichier(fd, STDOUT_FILENO);
    fprintf(stderr, "Octet lus: %d\n",octets_lus);
    return 0;
}
