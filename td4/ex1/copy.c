#include "sharemem.h"

#define BLKSIZE 1024

// take two file descriptors
int copiefichier (int f1, int f2){
    char buf[BLKSIZE];
    int octetlus;
    int octetecrits;
    int total_ect;
    for(;;){
        if((octetlus = read(f1, buf, BLKSIZE)) <= 0)
        // if we reach the end of the file, we break
        break;
        // write all we read to the second file
        if((octetecrits = write(f2, buf, octetlus)) == -1)
        break;
        total_ect += octetecrits;
    }
    return total_ect;
}