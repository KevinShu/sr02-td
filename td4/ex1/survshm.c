/*
Creation d'un segment memoire partage
le fils ecrit dans ce segment
le parent attent que le child soit termine et lire le contenu dans le segment
*/
#include "sharemem.h"
#include "copy.c"
#define BLKSIZE 1024
// defining the permission flags for the shared memory segment.
// set both read and write permissions for the owner of the shared memory segment.
#define PARAM (S_IRUSR|S_IWUSR)
#define SHMSIZE 27

// detach a shared memory segment and remove it from the system.
int detached_remove(int shmid, void *shmaddr){
    int error = 0;
    // detach shm
    if(shmdt(shmaddr) == -1){
        error = errno;
    }
    // remove shm
    if((shmctl(shmid, IPC_RMID, NULL)==-1) && !error){
        error = errno;
    }
    if(!error){
        return 0;
    }
    errno = error;
    return -1;
}

int main(int argc, char *argv[]){
    /* NEW VARIABLES*/
    int id;
    int *sharedtotal;
    int totalbytes = 0;
    /* END */

    int octets_lus;
    int child_pid;
    int fd, fd1, fd2;

    // NEW PART
    if((id = shmget(IPC_PRIVATE, SHMSIZE, PARAM)) == -1){
        perror("echec de creation d'un segment");
        return 1;
    }
    // utiisation de l'id obtenu par shmget pour attacher un segment de shm
    // The result of the shmat() call is cast to a pointer to an int
    // the sharedtotal pointer will point to the first int in the shared memory segment.
    if((sharedtotal = (int*)shmat(id, NULL, 0)) == (void*)-1){
        perror("echec de l'attachement au segment");
    }
    // END

    if (argc != 3){
        fprintf(stderr, "Usage: %s fichier1 fichier2", argv[0]);
        return 1;
    }
    if ((fd1 = open(argv[1], O_RDONLY)) == -1)
    {
        fprintf(stderr, "Echec d'ouverture du fichier %s\n", argv[1]);
        return 1;
    }
    if ((fd2 = open(argv[2], O_RDONLY)) == -1)
    { 
        fprintf(stderr, "Echec d'ouverture du fichier %s\n", argv[2]);
        return 1;
    }
    if((child_pid = fork()) == -1){
        // afficher un message en fonction de la valeur errno
        perror("echec de fork()");
        return 1;
    }
    if (child_pid > 0){
        /* PARENT CODE */
        fd = fd1;

    } else {
        /* CHILD CODE */
        fd = fd2;
    }
    // STDOUT_FILENO: descripteur du fichier de sortie standard
    octets_lus = copiefichier(fd, STDOUT_FILENO);
    // fprintf(stderr, "Octet lus: %d\n",octets_lus);
    totalbytes = octets_lus;

    // NEW
    // the child will write his byte count to the shm
    if(child_pid == 0){
        *sharedtotal = totalbytes;
        return 0;
        /* END OF CHILD PROCESS */
    }
    
    /* CODE REACHABLE BY PARENT */

    // wait for his child to finish
    // By passing NULL as the argument, 
    // the parent process is not interested in the termination status of the child process,
    // and is only waiting for it to terminate.
    // wait is returning the pid of the child that terminated

    /*
    The operating system sends a signal SIGCHLD to the parent process when the child process terminates, 
    and this signal causes the parent's wait system call to return. 
    The parent can then use the wait system call to obtain information about the child's exit status.
    */
    if(wait(NULL) == -1){
        perror("echec");
    } else {
        fprintf(stderr, "Octet copie:\n");
        fprintf(stderr, "%d par le pere\n",totalbytes);
        fprintf(stderr, "%d par le fils\n",*sharedtotal);
        fprintf(stderr, "octet total: %dn",totalbytes + *sharedtotal);
    }
    // detach from and remove the shared memory segment.
    if(detached_remove(id, sharedtotal) == -1){
        perror("echec");
    }

    return 0;
}