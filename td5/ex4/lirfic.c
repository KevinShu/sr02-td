#include <stdio.h>
#include <fcntl.h>

int main() {
    char str[100];
    int fd;
    fd = open("toto.txt", O_RDWR, 0666);
    for(;;){
    if((read(fd, str, 100)) <= 0)
        // if we reach the end of the file, we break
        break;
    }
    close(fd);
    for(int i = 0; str[i]!='\0'; i++){
        printf("%c", str[i]);
    }
}