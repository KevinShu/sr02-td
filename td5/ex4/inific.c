#include <stdio.h>
#include <fcntl.h>

int main() {
    int fd;
    fd = open("toto.txt", O_RDWR|O_CREAT|O_TRUNC, 0666);
    write(fd, "Bonjour, SR02", 13*sizeof(char));
    close(fd);
}