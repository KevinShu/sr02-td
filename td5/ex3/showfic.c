#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/mman.h>
#include <fcntl.h>

int main(){
    int* projection;
    int input;
    int fichier;
    long taille_fichier;
    struct stat etat_fichier;
    fichier = open("titi.dat", O_RDWR);
    stat("titi.dat", &etat_fichier);

    taille_fichier = etat_fichier.st_size;
    projection = (int*)mmap(NULL, taille_fichier, PROT_READ, MAP_SHARED, fichier, 0);
    if (projection == (int*)MAP_FAILED){
        perror("mmap");
        exit(0);
    }
    close(fichier);
    do{
        scanf("%d", &input);
        for(int i = 0; i< taille_fichier / 4; i++){
            printf("%d\n", projection[i]);
        }
    } while(input!=99);
    munmap((void*) projection, taille_fichier);
}