#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <unistd.h>     // For getopt()
#include <string.h>     // For strerror()

void sieve_of_eratosthenes(int n) {
    int i, j;
    int *prime = malloc(sizeof(int) * (n+1));

    for (i = 2; i <= n; i++) {
        prime[i] = 1;
    }

    for (i = 2; i <= sqrt(n); i++) {
        if (prime[i]) {
            for (j = i*i; j <= n; j += i) {
                prime[j] = 0;
            }
        }
    }

    for (i = 2; i <= n; i++) {
        if (prime[i]) {
            printf("%d ", i);
        }
    }
    
    free(prime);
}

int main(int argc, char *argv[]) {
    int n = 0;
    int opt;

    while((opt = getopt(argc, argv, "n:")) != -1) {
        switch(opt) {
            case 'n':
                n = atoi(optarg);
                break;
            default:
                fprintf(stderr, "Usage: %s -n [nb_sup_prime]\n", argv[0]);
                exit(EXIT_FAILURE);
        }
    }

    if(n == 0) {
        fprintf(stderr, "Option -n doit être fournie et son valeur entier supérieur à zero\n");
        exit(EXIT_FAILURE);
    }

    sieve_of_eratosthenes(n);
    printf("\n");
    return 0;
}
