#include <stdio.h>
#include <stdlib.h>
#include <math.h>

void sieve_of_eratosthenes(int n) {
    int i, j;
    int *prime = malloc(sizeof(int) * (n+1));

    // Initialisation: marquer tous les nombres comme premiers
    for (i = 2; i <= n; i++) {
        prime[i] = 1;
    }

    // Crible d'Ératosthène
    for (i = 2; i <= sqrt(n); i++) {
        if (prime[i]) {
            // Marquer les multiples de i comme non premiers
            for (j = i*i; j <= n; j += i) {
                prime[j] = 0;
            }
        }
    }

    // Afficher les nombres premiers
    for (i = 2; i <= n; i++) {
        if (prime[i]) {
            printf("%d ", i);
        }
    }
    
    // Libérer la mémoire
    free(prime);
}

int main() {
    int n;
    printf("Entrez une limite supérieure: ");
    scanf("%d", &n);
    sieve_of_eratosthenes(n);
    printf("\n");
    return 0;
}