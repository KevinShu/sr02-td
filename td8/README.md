# SR02 Devoir 3 Thread et Crible d’Eratosthenes

| **Auteurs**             |   **Contact**              |
|:------------------------| :------------- |
| Bingqian Shu            |  bingqian.shu@etu.utc.fr |
| M. Hicham LAKHLEF |  hicham.lakhlef@utc.fr |

## 0. Prérequis

Algorithme séquentiel
```
Input: un entier n > 1
Soit A un tableau de valeurs booléennes, indexées de 2 à n, et initialisées toutes à vrai. 
for i = 2, 3, 4, ..., √n :
    si A[i] est vrai:
        for j = i^2, i^2+i, i^2+2i, ..., n:
            A[j] := faux
Output : Maintenant tout i telle que A[i] est vrai est un nombre premier.
```

## 1. Tache 1
### 1.1 Dérouler l’exécution de cette algorithme avec n=20

Premièrement, on initialise un tableau A de taille 20, où toutes les valeurs sont définies comme 'vrai' (signifiant que chaque nombre est considéré comme premier jusqu'à preuve du contraire). Ensuite, on va vérifier chaque nombre de 2 à la racine carrée de 20 (≈4.47, donc jusqu'à 4), et si un nombre est considéré premier dans ce tableau, on va marquer tous ses multiples comme non-prime (faux).

Initialisation
```
2  3  4  5  6  7  8  9  10 11 12 13 14 15 16 17 18 19 20
V  V  V  V  V  V  V  V   V  V  V  V  V  V  V  V  V  V  V
```
i=2
```
2  3  4  5  6  7  8  9  10 11 12 13 14 15 16 17 18 19 20
V  V  F  V  F  V  F  V   F  V  F  V  F  V  F  V  F  V  F
```
i=3
```
2  3  4  5  6  7  8  9  10 11 12 13 14 15 16 17 18 19 20
V  V  F  V  F  V  F  F   F  V  F  V  F  F  F  V  F  V  F
```
i=4, comme 4 n'est pas premier, on ne fait rien.
i=5 > √20 fin de boucle

Les nombres restants marqués comme 'V' sont les nombres premiers inférieurs à 20 : 2, 3, 5, 7, 11, 13, 17, 19.

### 1.2 Pourquoi la boucle de l’intérieure (la deuxième boucle) commence à i^2 et pas 0 ou i ?

La boucle interne commence à i^2 parce que tous les nombres inférieurs à i^2 ont déjà été couverts par les multiples des nombres premiers inférieurs à i. Par exemple, quand i=3, tous les multiples de 2 (qui sont inférieurs à 3^2=9) ont déjà été marqués comme non premiers.

Plus précisément: 

Si nous commençons à 0, nous allons marquer chaque nombre comme non premier jusqu'à i^2. Cependant, si i > 2 (ce qui sera le cas après la première itération de la boucle externe), tous les nombres de 2 à i ont déjà été traités dans les itérations précédentes. Par conséquent, il est inutile de les traiter à nouveau, ce qui constitue un travail redondant.

Si nous commençons à i, nous allons marquer chaque nombre comme non premier de i à i^2. Mais encore une fois, tous les nombres inférieurs à i^2 ont déjà été traités dans les itérations précédentes, de sorte que marquer ces nombres comme non premiers constitue également un travail redondant.

(À chaque itération, nous prenons le prochain nombre premier non marqué, qui est i, et nous marquons tous ses multiples comme non premiers. Les multiples de i sont tous les nombres de la forme i*j, où j est un entier. Lorsque j < i, alors ij < i^2. Ces sont les multiples de i qui sont inférieurs à i^2. Mais pour chaque j < i, nous avons déjà fait une itération où i était égal à j. Donc, lors de ces itérations précédentes, nous avons déjà marqué tous les multiples de j, y compris ij.) 

## 1.3 Pourquoi la première boucle s’exécute jusqu’à √n? Que faites-vous si √n n’est pas un entier ?

La première boucle s'exécute jusqu'à √n parce que, pour tout nombre n, si n a un facteur supérieur à √n, alors ce facteur doit être multiplié par un autre facteur inférieur à √n pour obtenir n. Cela signifie qu'en vérifiant tous les nombres jusqu'à √n, nous couvrons tous les facteurs possibles de n. C'est la raison pour laquelle nous n'avons pas besoin de vérifier les nombres au-delà de √n, car tout facteur potentiel plus grand que √n aurait un partenaire correspondant qui est plus petit que √n.

Si √n n'est pas un entier, nous pouvons arrondir √n à l'entier le plus proche inférieur, ou utiliser la partie entière de √n. Par exemple, si n=20, alors √n est environ 4.47. Dans ce cas, nous utiliserions la partie entière, c'est-à-dire 4, comme limite supérieure de la boucle. Cela signifie que nous vérifions tous les nombres premiers jusqu'à 4. Cela couvre tous les facteurs potentiels de n, car tout facteur de 20 est soit 4 ou moins, soit a un partenaire qui est 4 ou moins.

## 2. Tache 2

Comme premier étape pour votre version parallèle du programme, implémenter une version séquentielle pour le crible d'Ératosthène.

### Réponse
```c
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

void sieve_of_eratosthenes(int n) {
    int i, j;
    int *prime = malloc(sizeof(int) * (n+1));

    // Initialisation: marquer tous les nombres comme premiers
    for (i = 2; i <= n; i++) {
        prime[i] = 1;
    }

    // Crible d'Ératosthène
    for (i = 2; i <= sqrt(n); i++) {
        if (prime[i]) {
            // Marquer les multiples de i comme non premiers
            for (j = i*i; j <= n; j += i) {
                prime[j] = 0;
            }
        }
    }

    // Afficher les nombres premiers
    for (i = 2; i <= n; i++) {
        if (prime[i]) {
            printf("%d ", i);
        }
    }
    
    // Libérer la mémoire
    free(prime);
}

int main() {
    int n;
    printf("Entrez une limite supérieure: ");
    scanf("%d", &n);
    sieve_of_eratosthenes(n);
    printf("\n");
    return 0;
}
```

### Résultat

```
Kevins-MacBook-Air:td8 shubq$ gcc -o tache2 tache2.c
Kevins-MacBook-Air:td8 shubq$ ./tache2
Entrez une limite supérieure: 20
2 3 5 7 11 13 17 19 
Kevins-MacBook-Air:td8 shubq$ ./tache2
Entrez une limite supérieure: 25
2 3 5 7 11 13 17 19 23 
Kevins-MacBook-Air:td8 shubq$ ./tache2
Entrez une limite supérieure: 500 
2 3 5 7 11 13 17 19 23 29 31 37 41 43 47 53 59 61 67 71 73 79 83 89 97 101 103 107 109 113 127 131 137 139 149 151 157 163 167 173 179 181 191 193 197 199 211 223 227 229 233 239 241 251 257 263 269 271 277 281 283 293 307 311 313 317 331 337 347 349 353 359 367 373 379 383 389 397 401 409 419 421 431 433 439 443 449 457 461 463 467 479 487 491 499 
Kevins-MacBook-Air:td8 shubq$ 
```


## 3. Tache 3

A partir de votre version séquentielle vous allez maintenant développer une version parallèle. L'idée est de paralléliser la boucle interne du pseudo code "for j = i2, i2+i, i2+2i, ..., n", en distribuant l'exploration de "i2, i2+i, i2+2i, ..., n" sur k threads. (k est un paramètre de votre implémentation.)

Pour éviter le coût de création répétée de threads, les k threads de votre programme doivent être créés en dehors de la boucle externe des algorithmes, et être réutilisés à chaque itération de la boucle externe.

Notes:
- Il y a 2 points de synchronisation dans ce code: Les threads de travail doivent attendre que le thread principal initialise le travail à effectuer (*); et le thread principal doit alors attendre que les threads de travail finissent leur part du travail (**).
- Pour répartir le travail entre les threads de travail, le thread principal indique à chacun d'eux quelle plage il faut couvrir. Pour ce faire, le thread principal doit définir cette plage (comme paramètre dans la fonction creat_thread) pour chaque thread de travail avant de débloquer le thread de travail. Chaque thread doit stocker la plage qu'il doit couvrir dans les attributs appropriés.

Une fois que vous avez implémenté une version parallèle du crible d'Eratosthenes, vérifiez cette version avec différentes valeurs de k (k = 1, 2, 3, 4, 7).

### Réponse

```c
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <math.h>

#define MAXN 100000000
#define MAXK 100

int prime[MAXN + 1];
int n, k;
pthread_t worker_tid[MAXK];

typedef struct {
    int start;
    int step;
    int prime;
    int working;
    pthread_cond_t cond;
} thread_data_t;

thread_data_t thread_data[MAXK];

pthread_mutex_t mutex;

void* worker(void* arg) {
    thread_data_t* data = (thread_data_t*) arg;

    while(1) {
        // Attendez d'être réveillé par le thread principal
        pthread_mutex_lock(&mutex);
        while(!data->working) {
            pthread_cond_wait(&data->cond, &mutex);
        }
        pthread_mutex_unlock(&mutex);

        // Si data->prime est -1, terminez le thread
        if(data->prime == -1) {
            return NULL;
        }

        // Faire son travail de recherche de prime
        for(int i = data->start; i <= n; i += data->step) {
            if(i != data->prime) {
                prime[i] = 0;
            }
        }

        // Signalez que le thread a terminé son travail courant
        pthread_mutex_lock(&mutex);
        data->working = 0;
        pthread_cond_signal(&data->cond);
        pthread_mutex_unlock(&mutex);
    }

    return NULL;
}

int main() {
    printf("Entrez une limite supérieure: ");
    scanf("%d", &n);
    printf("Entrez le nombre de threads: ");
    scanf("%d", &k);

    pthread_mutex_init(&mutex, NULL);

    // Initialiser le tableau prime
    for(int i = 2; i <= n; i++) {
        prime[i] = 1;
    }

    // Initialiser les données de thread
    for(int t = 0; t < k; t++) {
        thread_data[t].working = 0;
        pthread_cond_init(&thread_data[t].cond, NULL);
        pthread_create(&worker_tid[t], NULL, worker, &thread_data[t]);
    }

    // Pour chaque nombre i de 2 à sqrt(n)
    for(int i = 2; i * i <= n; i++) {
        if(prime[i]) {
            for(int t = 0; t < k; t++) {
                pthread_mutex_lock(&mutex);
                // Attendez que le thread t ait terminé son travail précédent
                while(thread_data[t].working) {
                    pthread_cond_wait(&thread_data[t].cond, &mutex);
                }
                // Donnez-lui son nouveau travail
                thread_data[t].start = i*i + t*i;
                thread_data[t].step = i*k;
                thread_data[t].prime = i;
                thread_data[t].working = 1;
                pthread_cond_signal(&thread_data[t].cond);
                pthread_mutex_unlock(&mutex);
            }
            // Attendez que tous les threads aient terminé
            for(int t = 0; t < k; t++) {
                pthread_mutex_lock(&mutex);
                while(thread_data[t].working) {
                    pthread_cond_wait(&thread_data[t].cond, &mutex);
                }
                pthread_mutex_unlock(&mutex);
            }
        }
    }

    // Signalez à tous les threads de terminer
    for(int t = 0; t < k; t++) {
        pthread_mutex_lock(&mutex);
        thread_data[t].prime = -1;
        thread_data[t].working = 1;
        pthread_cond_signal(&thread_data[t].cond);
        pthread_mutex_unlock(&mutex);
    }

    // Joignez tous les threads
    for(int t = 0; t < k; t++) {
        pthread_join(worker_tid[t], NULL);
    }

    // Afficher tous les nombres premiers
    for(int i = 2; i <= n; i++) {
        if(prime[i]) {
            printf("%d ", i);
        }
    }

    printf("\n");

    return 0;
}
```

### Résultat
```
Kevins-MacBook-Air:td8 shubq$ ./tache3
Entrez une limite supérieure: 100
Entrez le nombre de threads: 1
2 3 5 7 11 13 17 19 23 29 31 37 41 43 47 53 59 61 67 71 73 79 83 89 97 
Kevins-MacBook-Air:td8 shubq$ ./tache3
Entrez une limite supérieure: 300
Entrez le nombre de threads: 2
2 3 5 7 11 13 17 19 23 29 31 37 41 43 47 53 59 61 67 71 73 79 83 89 97 101 103 107 109 113 127 131 137 139 149 151 157 163 167 173 179 181 191 193 197 199 211 223 227 229 233 239 241 251 257 263 269 271 277 281 283 293 
Kevins-MacBook-Air:td8 shubq$ ./tache3
Entrez une limite supérieure: 150
Entrez le nombre de threads: 3
2 3 5 7 11 13 17 19 23 29 31 37 41 43 47 53 59 61 67 71 73 79 83 89 97 101 103 107 109 113 127 131 137 139 149 
Kevins-MacBook-Air:td8 shubq$ ./tache3
Entrez une limite supérieure: 450
Entrez le nombre de threads: 4
2 3 5 7 11 13 17 19 23 29 31 37 41 43 47 53 59 61 67 71 73 79 83 89 97 101 103 107 109 113 127 131 137 139 149 151 157 163 167 173 179 181 191 193 197 199 211 223 227 229 233 239 241 251 257 263 269 271 277 281 283 293 307 311 313 317 331 337 347 349 353 359 367 373 379 383 389 397 401 409 419 421 431 433 439 443 449 
Kevins-MacBook-Air:td8 shubq$ ./tache3
Entrez une limite supérieure: 120
Entrez le nombre de threads: 7
2 3 5 7 11 13 17 19 23 29 31 37 41 43 47 53 59 61 67 71 73 79 83 89 97 101 103 107 109 113 
```

Ce programme est constitué de trois grandes parties:
1.  La définition des structures et des variables globales:
- Les constantes `MAXN` et `MAXK` définissent respectivement le maximum de nombres que nous allons vérifier et le nombre maximal de threads que nous allons utiliser.
- Un tableau global prime qui stocke les informations sur le caractère premier ou non d'un nombre.
- Les identifiants de threads sont stockés dans `worker_tid`.
- La structure `thread_data_t` contient les informations nécessaires à chaque thread pour effectuer son travail. Le champ `start` indique où commencer la recherche, le champ `step` détermine combien augmenter à chaque itération, le champ `prime` stocke le nombre premier actuel. Le champ `working` est un drapeau pour indiquer si le thread est actuellement en train de travailler ou non. Le champ `cond` est une variable de condition qui est utilisée pour signaler au thread quand commencer à travailler.
2. La fonction `worker`:
- Chaque thread exécute cette fonction.
- Il s'agit d'une boucle infinie qui commence par attendre que le thread principal lui donne du travail.
- Une fois que le champ `working` du thread est mis à `1`, il commence à marquer tous les multiples de prime comme non-premiers dans le tableau prime.
- Il signale qu'il a terminé son travail en remettant `working` à `0` et en signalant sa condition.
3.  La fonction `main`:
- Cette fonction commence par initialiser le mutex et le tableau prime, puis elle crée les threads.
- Pour chaque nombre premier, elle donne du travail à chaque thread, attend que tous les threads aient terminé leur travail, puis passe au nombre premier suivant.
- À la fin, elle indique à tous les threads de terminer en mettant `prime` à `-1` et `working` à `1`, puis elle attend que tous les threads se terminent.
- Enfin, elle imprime tous les nombres premiers.


## 4. Tache 4

L'objectif de cette dernière tâche est de comparer les performances de vos versions successives et parallèles du crible d'Eratosthenes.
Mesurez le temps nécessaire pour calculer tous les nombres premiers ci-dessous 500,000 avec:
- Votre version séquentielle;
- Votre version parallèle avec le nombre de threads variant de 1 à 7.
Tracez les résultats sur un graphique. (Vous devriez exécuter vos expériences plusieurs fois pour obtenir des moyennes représentatives, vous saurez comment calculer un intervalle de confiance sur ces valeurs?)
Répétez la même mesure pour tous les nombres premiers inférieurs à 1 000 000; 2,000,000; 4 000 000, et dessinez les courbes correspondantes sur le même graphique. Comment interprétez-vous vos résultats?

### Réponse

Nous allons d'abord modifier le code dans la tache 3 pour qu'on puisse passer les valeurs (la variable n et la variable k) en paramètres en exécutant ce programme

Pour ce faire, nous allons ajouter les lignes suivantes:
```c
int main(int argc, char *argv[]) {
    int opt;

    while((opt = getopt(argc, argv, "n:k:")) != -1) {
        switch(opt) {
            case 'n':
                n = atoi(optarg);
                break;
            case 'k':
                k = atoi(optarg);
                break;
            default:
                fprintf(stderr, "Usage: %s -n [nb_sup_prime] -k [nb_thread]\n", argv[0]);
                exit(EXIT_FAILURE);
        }
    }

    if(n <= 0 || k <= 0) {
        fprintf(stderr, "Toutes les deux options -n et -k doivent être fournies et doivent être entier supérieur à zero\n");
        exit(EXIT_FAILURE);
    }

    ...

}
```

Nous faisons la même pour la tâche 2:
```c
...

void sieve_of_eratosthenes(int n) {
    ...
}

int main(int argc, char *argv[]) {
    int n = 0;
    int opt;

    while((opt = getopt(argc, argv, "n:")) != -1) {
        switch(opt) {
            case 'n':
                n = atoi(optarg);
                break;
            default:
                fprintf(stderr, "Usage: %s -n [nb_sup_prime]\n", argv[0]);
                exit(EXIT_FAILURE);
        }
    }

    if(n == 0) {
        fprintf(stderr, "Option -n doit être fournie et son valeur entier supérieur à zero\n");
        exit(EXIT_FAILURE);
    }

    sieve_of_eratosthenes(n);
    printf("\n");
    return 0;
}
```

Nous pouvons maintenant lancer le programme de façons suivante:
```
Kevins-MacBook-Air:td8 shubq$ ./tache4_sequentiel -n 100
2 3 5 7 11 13 17 19 23 29 31 37 41 43 47 53 59 61 67 71 73 79 83 89 97 

Kevins-MacBook-Air:td8 shubq$ ./tache4_parallel -n 250 -k 10
2 3 5 7 11 13 17 19 23 29 31 37 41 43 47 53 59 61 67 71 73 79 83 89 97 101 103 107 109 113 127 131 137 139 149 151 157 163 167 173 179 181 191 193 197 199 211 223 227 229 233 239 241

Kevins-MacBook-Air:td8 shubq$ ./tache4_parallel -n 300 -k 8
2 3 5 7 11 13 17 19 23 29 31 37 41 43 47 53 59 61 67 71 73 79 83 89 97 101 103 107 109 113 127 131 137 139 149 151 157 163 167 173 179 181 191 193 197 199 211 223 227 229 233 239 241 251 257 263 269 271 277 281 283 293 
```

Ensuite, nous écrivons un script shell pour lancer automatiquement nos deux version de recherche de prime

### Script de test - Version 1
```sh
#!/bin/bash

TIMEFORMAT=%R

for n in 500000 1000000 2000000 4000000 8000000 16000000 32000000 64000000 100000000
do
    echo "--------------------------------------"
    echo "Running sequential version for n=$n"
    elapsed_time=$( { time ./tache4_sequentiel -n $n > /dev/null; } 2>&1 )
    echo "Sequential time: $elapsed_time seconds"
    for k in {1..7}
    do
        echo "Running parallel version for n=$n and k=$k"
        elapsed_time=$( { time ./tache4_parallel -n $n -k $k > /dev/null; } 2>&1 )
        echo "Parallel time (k=$k): $elapsed_time seconds"
    done
done
```

```
Kevins-MacBook-Air:td8 shubq$ ./tache4_test.sh
--------------------------------------
Running sequential version for n=500000
Sequential time: 0.014 seconds
Running parallel version for n=500000 and k=1
Parallel time (k=1): 0.012 seconds
Running parallel version for n=500000 and k=2
Parallel time (k=2): 0.009 seconds
Running parallel version for n=500000 and k=3
Parallel time (k=3): 0.009 seconds
Running parallel version for n=500000 and k=4
Parallel time (k=4): 0.009 seconds
Running parallel version for n=500000 and k=5
Parallel time (k=5): 0.008 seconds
Running parallel version for n=500000 and k=6
Parallel time (k=6): 0.009 seconds
Running parallel version for n=500000 and k=7
Parallel time (k=7): 0.010 seconds
--------------------------------------
Running sequential version for n=1000000
Sequential time: 0.011 seconds
Running parallel version for n=1000000 and k=1
Parallel time (k=1): 0.013 seconds
Running parallel version for n=1000000 and k=2
Parallel time (k=2): 0.012 seconds
Running parallel version for n=1000000 and k=3
Parallel time (k=3): 0.012 seconds
Running parallel version for n=1000000 and k=4
Parallel time (k=4): 0.012 seconds
Running parallel version for n=1000000 and k=5
Parallel time (k=5): 0.014 seconds
Running parallel version for n=1000000 and k=6
Parallel time (k=6): 0.015 seconds
Running parallel version for n=1000000 and k=7
Parallel time (k=7): 0.015 seconds
--------------------------------------
Running sequential version for n=2000000
Sequential time: 0.020 seconds
Running parallel version for n=2000000 and k=1
Parallel time (k=1): 0.023 seconds
Running parallel version for n=2000000 and k=2
Parallel time (k=2): 0.021 seconds
Running parallel version for n=2000000 and k=3
Parallel time (k=3): 0.021 seconds
Running parallel version for n=2000000 and k=4
Parallel time (k=4): 0.022 seconds
Running parallel version for n=2000000 and k=5
Parallel time (k=5): 0.024 seconds
Running parallel version for n=2000000 and k=6
Parallel time (k=6): 0.025 seconds
Running parallel version for n=2000000 and k=7
Parallel time (k=7): 0.026 seconds
--------------------------------------
Running sequential version for n=4000000
Sequential time: 0.041 seconds
Running parallel version for n=4000000 and k=1
Parallel time (k=1): 0.044 seconds
Running parallel version for n=4000000 and k=2
Parallel time (k=2): 0.040 seconds
Running parallel version for n=4000000 and k=3
Parallel time (k=3): 0.040 seconds
Running parallel version for n=4000000 and k=4
Parallel time (k=4): 0.040 seconds
Running parallel version for n=4000000 and k=5
Parallel time (k=5): 0.042 seconds
Running parallel version for n=4000000 and k=6
Parallel time (k=6): 0.043 seconds
Running parallel version for n=4000000 and k=7
Parallel time (k=7): 0.045 seconds
--------------------------------------
Running sequential version for n=8000000
Sequential time: 0.087 seconds
Running parallel version for n=8000000 and k=1
Parallel time (k=1): 0.092 seconds
Running parallel version for n=8000000 and k=2
Parallel time (k=2): 0.083 seconds
Running parallel version for n=8000000 and k=3
Parallel time (k=3): 0.084 seconds
Running parallel version for n=8000000 and k=4
Parallel time (k=4): 0.080 seconds
Running parallel version for n=8000000 and k=5
Parallel time (k=5): 0.082 seconds
Running parallel version for n=8000000 and k=6
Parallel time (k=6): 0.087 seconds
Running parallel version for n=8000000 and k=7
Parallel time (k=7): 0.087 seconds
--------------------------------------
Running sequential version for n=16000000
Sequential time: 0.183 seconds
Running parallel version for n=16000000 and k=1
Parallel time (k=1): 0.192 seconds
Running parallel version for n=16000000 and k=2
Parallel time (k=2): 0.170 seconds
Running parallel version for n=16000000 and k=3
Parallel time (k=3): 0.165 seconds
Running parallel version for n=16000000 and k=4
Parallel time (k=4): 0.160 seconds
Running parallel version for n=16000000 and k=5
Parallel time (k=5): 0.168 seconds
Running parallel version for n=16000000 and k=6
Parallel time (k=6): 0.170 seconds
Running parallel version for n=16000000 and k=7
Parallel time (k=7): 0.174 seconds
--------------------------------------
Running sequential version for n=32000000
Sequential time: 0.381 seconds
Running parallel version for n=32000000 and k=1
Parallel time (k=1): 0.419 seconds
Running parallel version for n=32000000 and k=2
Parallel time (k=2): 0.349 seconds
Running parallel version for n=32000000 and k=3
Parallel time (k=3): 0.330 seconds
Running parallel version for n=32000000 and k=4
Parallel time (k=4): 0.322 seconds
Running parallel version for n=32000000 and k=5
Parallel time (k=5): 0.342 seconds
Running parallel version for n=32000000 and k=6
Parallel time (k=6): 0.346 seconds
Running parallel version for n=32000000 and k=7
Parallel time (k=7): 0.345 seconds
--------------------------------------
Running sequential version for n=64000000
Sequential time: 0.797 seconds
Running parallel version for n=64000000 and k=1
Parallel time (k=1): 0.887 seconds
Running parallel version for n=64000000 and k=2
Parallel time (k=2): 0.702 seconds
Running parallel version for n=64000000 and k=3
Parallel time (k=3): 0.765 seconds
Running parallel version for n=64000000 and k=4
Parallel time (k=4): 0.724 seconds
Running parallel version for n=64000000 and k=5
Parallel time (k=5): 0.782 seconds
Running parallel version for n=64000000 and k=6
Parallel time (k=6): 0.761 seconds
Running parallel version for n=64000000 and k=7
Parallel time (k=7): 0.794 seconds
--------------------------------------
Running sequential version for n=100000000
Sequential time: 1.303 seconds
Running parallel version for n=100000000 and k=1
Parallel time (k=1): 1.366 seconds
Running parallel version for n=100000000 and k=2
Parallel time (k=2): 1.187 seconds
Running parallel version for n=100000000 and k=3
Parallel time (k=3): 1.163 seconds
Running parallel version for n=100000000 and k=4
Parallel time (k=4): 1.106 seconds
Running parallel version for n=100000000 and k=5
Parallel time (k=5): 1.147 seconds
Running parallel version for n=100000000 and k=6
Parallel time (k=6): 1.153 seconds
Running parallel version for n=100000000 and k=7
Parallel time (k=7): 1.152 seconds
```

### Script de test - Version 2
En se basant sur version 1, ajoutons un paramètre qui nous permettrai de répéter le test N fois puis obtenir le temps moyenne et l'intervalle de confiance (à 95%)
```sh
#!/bin/bash

TIMEFORMAT=%R

N=1
while getopts "N:" opt; do
  case ${opt} in
    N )
      N=$OPTARG
      ;;
    \? )
      echo "Invalid option: $OPTARG" 1>&2
      ;;
    : )
      echo "Invalid option: $OPTARG requires an argument" 1>&2
      ;;
  esac
done
shift $((OPTIND -1))

for n in 500000 1000000 2000000 4000000 8000000 16000000 32000000 64000000
do
    echo "--------------------------------------"
    echo "Running sequential version for n=$n"

    for i in $(seq $N)
    do
        echo $( { time ./tache4_sequentiel -n $n > /dev/null; } 2>&1 ) >> sequential_times.txt
    done
    mean=$(awk '{sum+=$1; sumsq+=$1*$1} END {print sum/NR, sqrt(sumsq/NR - (sum/NR)^2)}' sequential_times.txt)
    rm sequential_times.txt
    mean_value=$(echo $mean | cut -d ' ' -f 1)
    std_dev=$(echo $mean | cut -d ' ' -f 2)
    confidence_interval=$(echo "scale=5; 1.96*$std_dev/sqrt($N)" | bc -l)
    echo "Sequential average time: $mean_value seconds, interval of confidence: $confidence_interval"

    for k in {1..7}
    do
        echo "Running parallel version for n=$n and k=$k"
        for i in $(seq $N)
        do
            echo $( { time ./tache4_parallel -n $n -k $k > /dev/null; } 2>&1 ) >> parallel_times.txt
        done
        mean=$(awk '{sum+=$1; sumsq+=$1*$1} END {print sum/NR, sqrt(sumsq/NR - (sum/NR)^2)}' parallel_times.txt)
        rm parallel_times.txt
        mean_value=$(echo $mean | cut -d ' ' -f 1)
        std_dev=$(echo $mean | cut -d ' ' -f 2)
        confidence_interval=$(echo "scale=5; 1.96*$std_dev/sqrt($N)" | bc -l)
        echo "Parallel average time (k=$k): $mean_value seconds, interval of confidence: $confidence_interval"
    done
done
```

Après avoir exécuté cette version, nous allons obtenir le temps moyenne d'exécution pour chaque n est k ainsi que sa intervalle de confiance de 95%
```
Kevins-MacBook-Air:td8 shubq$ ./tache4_test2.sh -N 5
--------------------------------------
Running sequential version for n=500000
Sequential average time: 0.0088 seconds, interval of confidence: .00150
Running parallel version for n=500000 and k=1
Parallel average time (k=1): 0.0072 seconds, interval of confidence: .00034
Running parallel version for n=500000 and k=2
Parallel average time (k=2): 0.0064 seconds, interval of confidence: .00042
Running parallel version for n=500000 and k=3
Parallel average time (k=3): 0.0064 seconds, interval of confidence: .00042
Running parallel version for n=500000 and k=4
Parallel average time (k=4): 0.0072 seconds, interval of confidence: .00034
Running parallel version for n=500000 and k=5
Parallel average time (k=5): 0.0076 seconds, interval of confidence: .00042
Running parallel version for n=500000 and k=6
Parallel average time (k=6): 0.0082 seconds, interval of confidence: .00034
Running parallel version for n=500000 and k=7
Parallel average time (k=7): 0.0086 seconds, interval of confidence: .00069
--------------------------------------
Running sequential version for n=1000000
Sequential average time: 0.01 seconds, interval of confidence: 0
Running parallel version for n=1000000 and k=1
Parallel average time (k=1): 0.0122 seconds, interval of confidence: .00034
Running parallel version for n=1000000 and k=2
Parallel average time (k=2): 0.0114 seconds, interval of confidence: .00042
Running parallel version for n=1000000 and k=3
Parallel average time (k=3): 0.0114 seconds, interval of confidence: .00042
Running parallel version for n=1000000 and k=4
Parallel average time (k=4): 0.0126 seconds, interval of confidence: .00069
Running parallel version for n=1000000 and k=5
Parallel average time (k=5): 0.0124 seconds, interval of confidence: .00069
Running parallel version for n=1000000 and k=6
Parallel average time (k=6): 0.0132 seconds, interval of confidence: .00034
Running parallel version for n=1000000 and k=7
Parallel average time (k=7): 0.0134 seconds, interval of confidence: .00069
--------------------------------------
Running sequential version for n=2000000
Sequential average time: 0.02 seconds, interval of confidence: 0
Running parallel version for n=2000000 and k=1
Parallel average time (k=1): 0.023 seconds, interval of confidence: 0
Running parallel version for n=2000000 and k=2
Parallel average time (k=2): 0.0202 seconds, interval of confidence: .00034
Running parallel version for n=2000000 and k=3
Parallel average time (k=3): 0.02 seconds, interval of confidence: .00055
Running parallel version for n=2000000 and k=4
Parallel average time (k=4): 0.0212 seconds, interval of confidence: .00128
Running parallel version for n=2000000 and k=5
Parallel average time (k=5): 0.0224 seconds, interval of confidence: .00069
Running parallel version for n=2000000 and k=6
Parallel average time (k=6): 0.0292 seconds, interval of confidence: .01003
Running parallel version for n=2000000 and k=7
Parallel average time (k=7): 0.0246 seconds, interval of confidence: .00089
--------------------------------------
Running sequential version for n=4000000
Sequential average time: 0.038 seconds, interval of confidence: 0
Running parallel version for n=4000000 and k=1
Parallel average time (k=1): 0.0442 seconds, interval of confidence: .00034
Running parallel version for n=4000000 and k=2
Parallel average time (k=2): 0.04 seconds, interval of confidence: 0
Running parallel version for n=4000000 and k=3
Parallel average time (k=3): 0.0394 seconds, interval of confidence: .00042
Running parallel version for n=4000000 and k=4
Parallel average time (k=4): 0.04 seconds, interval of confidence: .00078
Running parallel version for n=4000000 and k=5
Parallel average time (k=5): 0.0422 seconds, interval of confidence: .00065
Running parallel version for n=4000000 and k=6
Parallel average time (k=6): 0.0434 seconds, interval of confidence: .00042
Running parallel version for n=4000000 and k=7
Parallel average time (k=7): 0.0438 seconds, interval of confidence: .00065
--------------------------------------
Running sequential version for n=8000000
Sequential average time: 0.0836 seconds, interval of confidence: .00069
Running parallel version for n=8000000 and k=1
Parallel average time (k=1): 0.0948 seconds, interval of confidence: .00178
Running parallel version for n=8000000 and k=2
Parallel average time (k=2): 0.084 seconds, interval of confidence: .00156
Running parallel version for n=8000000 and k=3
Parallel average time (k=3): 0.082 seconds, interval of confidence: .00055
Running parallel version for n=8000000 and k=4
Parallel average time (k=4): 0.0814 seconds, interval of confidence: .00152
Running parallel version for n=8000000 and k=5
Parallel average time (k=5): 0.084 seconds, interval of confidence: 0
Running parallel version for n=8000000 and k=6
Parallel average time (k=6): 0.0856 seconds, interval of confidence: .00089
Running parallel version for n=8000000 and k=7
Parallel average time (k=7): 0.087 seconds, interval of confidence: .00156
--------------------------------------
Running sequential version for n=16000000
Sequential average time: 0.1812 seconds, interval of confidence: .00160
Running parallel version for n=16000000 and k=1
Parallel average time (k=1): 0.1952 seconds, interval of confidence: .00128
Running parallel version for n=16000000 and k=2
Parallel average time (k=2): 0.1752 seconds, interval of confidence: .00742
Running parallel version for n=16000000 and k=3
Parallel average time (k=3): 0.1708 seconds, interval of confidence: .00290
Running parallel version for n=16000000 and k=4
Parallel average time (k=4): 0.1622 seconds, interval of confidence: .00237
Running parallel version for n=16000000 and k=5
Parallel average time (k=5): 0.169 seconds, interval of confidence: .00078
Running parallel version for n=16000000 and k=6
Parallel average time (k=6): 0.1754 seconds, interval of confidence: .00251
Running parallel version for n=16000000 and k=7
Parallel average time (k=7): 0.1756 seconds, interval of confidence: .00225
--------------------------------------
Running sequential version for n=32000000
Sequential average time: 0.3758 seconds, interval of confidence: .00679
Running parallel version for n=32000000 and k=1
Parallel average time (k=1): 0.403 seconds, interval of confidence: .00135
Running parallel version for n=32000000 and k=2
Parallel average time (k=2): 0.3598 seconds, interval of confidence: .00993
Running parallel version for n=32000000 and k=3
Parallel average time (k=3): 0.3348 seconds, interval of confidence: .00329
Running parallel version for n=32000000 and k=4
Parallel average time (k=4): 0.3284 seconds, interval of confidence: .00225
Running parallel version for n=32000000 and k=5
Parallel average time (k=5): 0.3462 seconds, interval of confidence: .00541
Running parallel version for n=32000000 and k=6
Parallel average time (k=6): 0.3474 seconds, interval of confidence: .00547
Running parallel version for n=32000000 and k=7
Parallel average time (k=7): 0.3856 seconds, interval of confidence: .00969
--------------------------------------
Running sequential version for n=64000000
Sequential average time: 0.8028 seconds, interval of confidence: .01957
Running parallel version for n=64000000 and k=1
Parallel average time (k=1): 0.8218 seconds, interval of confidence: .00732
Running parallel version for n=64000000 and k=2
Parallel average time (k=2): 0.7114 seconds, interval of confidence: .01599
Running parallel version for n=64000000 and k=3
Parallel average time (k=3): 0.6952 seconds, interval of confidence: .02508
Running parallel version for n=64000000 and k=4
Parallel average time (k=4): 0.693 seconds, interval of confidence: .02024
Running parallel version for n=64000000 and k=5
Parallel average time (k=5): 0.7238 seconds, interval of confidence: .01754
Running parallel version for n=64000000 and k=6
Parallel average time (k=6): 0.7178 seconds, interval of confidence: .01100
Running parallel version for n=64000000 and k=7
Parallel average time (k=7): 0.7484 seconds, interval of confidence: .01659
```

### Script de test - Version 3
À partir de version 2, nous allons stocké les données de test obtenu dans un ficher, puis utiliser le programme `gnuplot` pour tracer des graphique
```sh
#!/bin/bash

TIMEFORMAT=%R

N=1
while getopts "N:" opt; do
  case ${opt} in
    N )
      N=$OPTARG
      ;;
    \? )
      echo "Invalid option: $OPTARG" 1>&2
      ;;
    : )
      echo "Invalid option: $OPTARG requires an argument" 1>&2
      ;;
  esac
done
shift $((OPTIND -1))

# Initialise the plot data file with headers
echo -e "n\tSequential\tParallel_k=1\tParallel_k=2\tParallel_k=3\tParallel_k=4\tParallel_k=5\tParallel_k=6\tParallel_k=7" > plotdata.txt

for n in 500000 1000000 2000000 4000000 8000000 16000000 32000000 64000000
do
    echo "--------------------------------------"
    echo "Running sequential version for n=$n"
    line="$n" # start a new line of data with the current value of n

    for i in $(seq $N)
    do
        echo $( { time ./tache4_sequentiel -n $n > /dev/null; } 2>&1 ) >> sequential_times.txt
    done
    mean=$(awk '{sum+=$1; sumsq+=$1*$1} END {print sum/NR, sqrt(sumsq/NR - (sum/NR)^2)}' sequential_times.txt)
    rm sequential_times.txt
    mean_value=$(echo $mean | cut -d ' ' -f 1)
    std_dev=$(echo $mean | cut -d ' ' -f 2)
    confidence_interval=$(echo "scale=5; 1.96*$std_dev/sqrt($N)" | bc -l)
    echo "Sequential average time: $mean_value seconds, interval of confidence: $confidence_interval"
    line="$line\t$mean_value"

    for k in {1..7}
    do
        echo "Running parallel version for n=$n and k=$k"
        for i in $(seq $N)
        do
            echo $( { time ./tache4_parallel -n $n -k $k > /dev/null; } 2>&1 ) >> parallel_times.txt
        done
        mean=$(awk '{sum+=$1; sumsq+=$1*$1} END {print sum/NR, sqrt(sumsq/NR - (sum/NR)^2)}' parallel_times.txt)
        rm parallel_times.txt
        mean_value=$(echo $mean | cut -d ' ' -f 1)
        std_dev=$(echo $mean | cut -d ' ' -f 2)
        confidence_interval=$(echo "scale=5; 1.96*$std_dev/sqrt($N)" | bc -l)
        echo "Parallel average time (k=$k): $mean_value seconds, interval of confidence: $confidence_interval"
        line="$line\t$mean_value"
    done
    echo -e "$line" >> plotdata.txt
done
```

Le fichier `plotdata.txt` que nous avons obtenu en exécutant le script au-dessus
```
n	Sequential	Parallel_k=1	Parallel_k=2	Parallel_k=3	Parallel_k=4	Parallel_k=5	Parallel_k=6	Parallel_k=7
500000	0.012	0.009	0.007	0.007	0.008	0.009	0.008	0.009
1000000	0.01	0.012	0.012	0.012	0.013	0.013	0.014	0.014
2000000	0.02	0.023	0.021	0.02	0.022	0.023	0.024	0.025
4000000	0.038	0.045	0.04	0.04	0.04	0.043	0.043	0.044
8000000	0.091	0.095	0.085	0.083	0.083	0.085	0.113	0.087
16000000	0.182	0.196	0.174	0.17	0.164	0.172	0.176	0.175
32000000	0.375	0.405	0.348	0.344	0.365	0.346	0.355	0.36
64000000	0.787	0.82	0.734	0.748	0.733	0.754	0.77	0.813
```

Nous créons un autre script pour tracer une graphe gnuplot en utilisant ces données
```sh
#!/opt/homebrew/bin/gnuplot

set term png
set output "plot.png"
set title "Temps moyen en fonction de n"
set xlabel "n"
set ylabel "Temps moyen (secondes)"
set grid

plot "plotdata.txt" using 1:2 with linespoints title "seq", \
     "plotdata.txt" using 1:3 with linespoints title "k=1", \
     "plotdata.txt" using 1:4 with linespoints title "k=2", \
     "plotdata.txt" using 1:5 with linespoints title "k=3", \
     "plotdata.txt" using 1:6 with linespoints title "k=4", \
     "plotdata.txt" using 1:7 with linespoints title "k=5", \
     "plotdata.txt" using 1:8 with linespoints title "k=6", \
     "plotdata.txt" using 1:9 with linespoints title "k=7", \
```

Notre résultat final
<img src="./plot.png" alt="plot-img" width="800"/>

Nous voyons dans la graphe que:
- Utiliser qu'un seul thread (k=1) n'est généralement pas une bonne idée
- La version séquentielle a un peu d'avantage quand le n est petit
- Globalement, la version parallèle avec plus de thread a l'avantage, notamment quand n est grand
- Si on se limite dans notre contexte et dans le résultat de ce test, k=4 et k=5 sont globalement plus performants.

## 5. Tache 5

Analyser l'impact des deux optimisations suivantes :
- Accélérer la boucle interne: Votre boucle interne utilise un pas de i. Comment pourriez-vous le modifier pour
utiliser un pas de 2i au lieu de i ? (Indice: vous devrez considérer 2 comme un cas distinct.)
- Réduction de l’espace mémoire: votre implémentation utilise un tableau de Boolean (disons isPrime []) pour se souvenir quels nombres sont premiers. Cette approche utilise beaucoup d'espace: tous les nombres pairs (donc presque la moitié du tableau) sauf 2 seront mis à faux. Puisque nous savons déjà que (i) 0 et 1 ne sont pas premiers (par définition), et que (ii) tous les nombres pairs sauf 2 (i.e. 4,6,8, ...) ne sont pas premiers non plus, une approche plus efficace consiste à en utilisant un tableau qui est moitié moins grand, et en maintenant seulement un booléen pour les nombres impairs au-dessus de 3. En d'autres termes, vous pouvez utiliser isPrime [i] pour indiquer si k = 2 * i + 3 est premier ou non.

### Optimisation 1

Pour appliquer l'optimisation 1, qui consiste à accélérer la boucle interne en utilisant un pas de 2i au lieu de i, voici la modification à apporter au code:
```c
......

void* worker(void* arg) {

    ......

        // i+=2*data->step au lieu de i+=data->step
        if (data->prime != 2) {
            for (int i = data->start; i <= n; i += 2 * data->step) {
                if (i != data->prime) {
                    prime[i] = 0;
                }
            }
        } else {
            // le cas particuliere 2
            for (int i = data->start; i <= n; i += data->step) {
                if (i != data->prime) {
                    prime[i] = 0;
                }
            }
        }

        ......

}
```

Dans cette version modifiée, la boucle interne du worker utilise `2 * data->step` comme pas pour itérer sur chaque deuxième multiple de `data->prime`. Cependant, afin de gérer le cas particulier de `2`, une condition if est ajoutée pour traiter séparément les multiples pairs de `2`, car ils doivent être marqués comme non premiers. Le résultat d'exécution restera le même que celui dans tâche 3.

### Optimisation 2
Dans cette version d'optimisation :
- Le tableau prime a été réduit ainsi que sa initialisation dans `main`
```c
// AVANT
int prime[MAXN + 1];

for (int i = 2; i <= n; i++) {
    prime[i] = 1;
}
// APRES
int prime[(MAXN - 1) / 2 + 1];

for (int i = 0; i <= (n - 1) / 2; i++) {
    prime[i] = 1;
}
```
- Le calcul de pas a été modifié dans `worker`
```c
// AVANT
if (data->prime != 2) {
    for (int i = data->start; i <= n; i += 2 * data->step) {
        if (i != data->prime) {
            prime[i] = 0;
        }
    }
} else {
    for (int i = data->start; i <= n; i += data->step) {
        if (i != data->prime) {
            prime[i] = 0;
        }
    }
}
//APRES
for (int i = data->start; i <= n; i += 2 * data->step) {
            int idx = (i - 3) / 2;
            if (idx >= 0 && prime[idx]) {
                prime[idx] = 0;
            }
}
```
- Le calcul de répartition a été modifié dans `main`
```c
//AVANT
for (int i = 2; i * i <= n; i++) {
    if (prime[i]) {
        for (int t = 0; t < k; t++) {
            ......
            thread_data[t].start = i * i + t * i;
            ......
        }

        ......
    }
}
//APRES
 for (int i = 3; i * i <= n; i += 2) {
    int idx = (i - 3) / 2;
    if (prime[idx]) {
        for (int t = 0; t < k; t++) {
            ......
            thread_data[t].start = i * i + 2 * t * i;
            ......
        }

        ......
    }
}
```
- L'affichage de prime a été modifié
```c
//AVANT
for (int i = 2; i <= n; i++) {
    if (prime[i]) {
        printf("%d ", i);
    }
}
//APRES
printf("2 ");
for (int i = 0; i <= (n - 1) / 2 - 1; i++) {
    if (prime[i]) {
        printf("%d ", 2 * i + 3);
    }
}
```
Le résultat d'exécution restera le même que celui dans tâche 3.