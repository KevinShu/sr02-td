#!/bin/bash

TIMEFORMAT=%R

N=1
while getopts "N:" opt; do
  case ${opt} in
    N )
      N=$OPTARG
      ;;
    \? )
      echo "Invalid option: $OPTARG" 1>&2
      ;;
    : )
      echo "Invalid option: $OPTARG requires an argument" 1>&2
      ;;
  esac
done
shift $((OPTIND -1))

# Initialise the plot data file with headers
echo -e "n\tSequential\tParallel_k=1\tParallel_k=2\tParallel_k=3\tParallel_k=4\tParallel_k=5\tParallel_k=6\tParallel_k=7" > plotdata.txt

for n in 500000 1000000 2000000 4000000 8000000 16000000 32000000 64000000
do
    echo "--------------------------------------"
    echo "Running sequential version for n=$n"
    line="$n" # start a new line of data with the current value of n

    for i in $(seq $N)
    do
        echo $( { time ./tache4_sequentiel -n $n > /dev/null; } 2>&1 ) >> sequential_times.txt
    done
    mean=$(awk '{sum+=$1; sumsq+=$1*$1} END {print sum/NR, sqrt(sumsq/NR - (sum/NR)^2)}' sequential_times.txt)
    rm sequential_times.txt
    mean_value=$(echo $mean | cut -d ' ' -f 1)
    std_dev=$(echo $mean | cut -d ' ' -f 2)
    confidence_interval=$(echo "scale=5; 1.96*$std_dev/sqrt($N)" | bc -l)
    echo "Sequential average time: $mean_value seconds, interval of confidence: $confidence_interval"
    line="$line\t$mean_value"

    for k in {1..7}
    do
        echo "Running parallel version for n=$n and k=$k"
        for i in $(seq $N)
        do
            echo $( { time ./tache4_parallel -n $n -k $k > /dev/null; } 2>&1 ) >> parallel_times.txt
        done
        mean=$(awk '{sum+=$1; sumsq+=$1*$1} END {print sum/NR, sqrt(sumsq/NR - (sum/NR)^2)}' parallel_times.txt)
        rm parallel_times.txt
        mean_value=$(echo $mean | cut -d ' ' -f 1)
        std_dev=$(echo $mean | cut -d ' ' -f 2)
        confidence_interval=$(echo "scale=5; 1.96*$std_dev/sqrt($N)" | bc -l)
        echo "Parallel average time (k=$k): $mean_value seconds, interval of confidence: $confidence_interval"
        line="$line\t$mean_value"
    done
    echo -e "$line" >> plotdata.txt
done