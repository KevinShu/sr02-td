#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <math.h>
#include <unistd.h>     // For getopt()
#include <string.h>     // For strerror()

#define MAXN 100000000
#define MAXK 100

int prime[MAXN + 1];
int n, k;
pthread_t worker_tid[MAXK];

typedef struct {
    int start;
    int step;
    int prime;
    int working;
    pthread_cond_t cond;
} thread_data_t;

thread_data_t thread_data[MAXK];

pthread_mutex_t mutex;

void* worker(void* arg) {
    thread_data_t* data = (thread_data_t*) arg;

    while (1) {
        pthread_mutex_lock(&mutex);
        while (!data->working) {
            pthread_cond_wait(&data->cond, &mutex);
        }
        pthread_mutex_unlock(&mutex);

        if (data->prime == -1) {
            return NULL;
        }

        if (data->prime != 2) {
            for (int i = data->start; i <= n; i += 2 * data->step) {
                if (i != data->prime) {
                    prime[i] = 0;
                }
            }
        } else {
            for (int i = data->start; i <= n; i += data->step) {
                if (i != data->prime) {
                    prime[i] = 0;
                }
            }
        }

        pthread_mutex_lock(&mutex);
        data->working = 0;
        pthread_cond_signal(&data->cond);
        pthread_mutex_unlock(&mutex);
    }

    return NULL;
}

int main(int argc, char *argv[]) {
    int opt;

    while ((opt = getopt(argc, argv, "n:k:")) != -1) {
        switch (opt) {
            case 'n':
                n = atoi(optarg);
                break;
            case 'k':
                k = atoi(optarg);
                break;
            default:
                fprintf(stderr, "Usage: %s -n [nb_sup_prime] -k [nb_thread]\n", argv[0]);
                exit(EXIT_FAILURE);
        }
    }

    if (n <= 0 || k <= 0) {
        fprintf(stderr, "Toutes les deux options -n et -k doivent être fournies et doivent être un entier supérieur à zéro\n");
        exit(EXIT_FAILURE);
    }

    pthread_mutex_init(&mutex, NULL);

    for (int i = 2; i <= n; i++) {
        prime[i] = 1;
    }

    for (int t = 0; t < k; t++) {
        thread_data[t].working = 0;
        pthread_cond_init(&thread_data[t].cond, NULL);
        pthread_create(&worker_tid[t], NULL, worker, &thread_data[t]);
    }

    for (int i = 2; i * i <= n; i++) {
        if (prime[i]) {
            for (int t = 0; t < k; t++) {
                pthread_mutex_lock(&mutex);
                while (thread_data[t].working) {
                    pthread_cond_wait(&thread_data[t].cond, &mutex);
                }
                thread_data[t].start = i * i + t * i;
                thread_data[t].step = i * k;
                thread_data[t].prime = i;
                thread_data[t].working = 1;
                pthread_cond_signal(&thread_data[t].cond);
                pthread_mutex_unlock(&mutex);
            }

            for (int t = 0; t < k; t++) {
                pthread_mutex_lock(&mutex);
                while (thread_data[t].working) {
                    pthread_cond_wait(&thread_data[t].cond, &mutex);
                }
                pthread_mutex_unlock(&mutex);
            }
        }
    }

    for (int t = 0; t < k; t++) {
        pthread_mutex_lock(&mutex);
        thread_data[t].prime = -1;
        thread_data[t].working = 1;
        pthread_cond_signal(&thread_data[t].cond);
        pthread_mutex_unlock(&mutex);
    }

    for (int t = 0; t < k; t++) {
        pthread_join(worker_tid[t], NULL);
    }

    for (int i = 2; i <= n; i++) {
        if (prime[i]) {
            printf("%d ", i);
        }
    }

    printf("\n");

    return 0;
}