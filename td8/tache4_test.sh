#!/bin/bash

TIMEFORMAT=%R

for n in 500000 1000000 2000000 4000000 8000000 16000000 32000000 64000000 100000000
do
    echo "--------------------------------------"
    echo "Running sequential version for n=$n"
    elapsed_time=$( { time ./tache4_sequentiel -n $n > /dev/null; } 2>&1 )
    echo "Sequential time: $elapsed_time seconds"
    for k in {1..7}
    do
        echo "Running parallel version for n=$n and k=$k"
        elapsed_time=$( { time ./tache4_parallel -n $n -k $k > /dev/null; } 2>&1 )
        echo "Parallel time (k=$k): $elapsed_time seconds"
    done
done