#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include <math.h>

#define MAXN 1000000
#define MAXK 100

int prime[MAXN + 1];
int n, k;
pthread_t worker_tid[MAXK];

typedef struct {
    int start;
    int step;
    int prime;
    int working;
    pthread_cond_t cond;
} thread_data_t;

thread_data_t thread_data[MAXK];

pthread_mutex_t mutex;

void* worker(void* arg) {
    thread_data_t* data = (thread_data_t*) arg;

    while(1) {
        // Attendez d'être réveillé par le thread principal
        pthread_mutex_lock(&mutex);
        while(!data->working) {
            pthread_cond_wait(&data->cond, &mutex);
        }
        pthread_mutex_unlock(&mutex);

        // Si data->prime est -1, terminez le thread
        if(data->prime == -1) {
            return NULL;
        }

        // Faire son travail de recherche de prime
        for(int i = data->start; i <= n; i += data->step) {
            if(i != data->prime) {
                prime[i] = 0;
            }
        }

        // Signalez que le thread a terminé son travail courant
        pthread_mutex_lock(&mutex);
        data->working = 0;
        pthread_cond_signal(&data->cond);
        pthread_mutex_unlock(&mutex);
    }

    return NULL;
}

int main() {
    printf("Entrez une limite supérieure: ");
    scanf("%d", &n);
    printf("Entrez le nombre de threads: ");
    scanf("%d", &k);

    pthread_mutex_init(&mutex, NULL);

    // Initialiser le tableau prime
    for(int i = 2; i <= n; i++) {
        prime[i] = 1;
    }

    // Initialiser les données de thread
    for(int t = 0; t < k; t++) {
        thread_data[t].working = 0;
        pthread_cond_init(&thread_data[t].cond, NULL);
        pthread_create(&worker_tid[t], NULL, worker, &thread_data[t]);
    }

    // Pour chaque nombre i de 2 à sqrt(n)
    for(int i = 2; i * i <= n; i++) {
        if(prime[i]) {
            for(int t = 0; t < k; t++) {
                pthread_mutex_lock(&mutex);
                // Attendez que le thread t ait terminé son travail précédent
                while(thread_data[t].working) {
                    pthread_cond_wait(&thread_data[t].cond, &mutex);
                }
                // Donnez-lui son nouveau travail
                thread_data[t].start = i*i + t*i;
                thread_data[t].step = i*k;
                thread_data[t].prime = i;
                thread_data[t].working = 1;
                pthread_cond_signal(&thread_data[t].cond);
                pthread_mutex_unlock(&mutex);
            }
            // Attendez que tous les threads aient terminé
            for(int t = 0; t < k; t++) {
                pthread_mutex_lock(&mutex);
                while(thread_data[t].working) {
                    pthread_cond_wait(&thread_data[t].cond, &mutex);
                }
                pthread_mutex_unlock(&mutex);
            }
        }
    }

    // Signalez à tous les threads de terminer
    for(int t = 0; t < k; t++) {
        pthread_mutex_lock(&mutex);
        thread_data[t].prime = -1;
        thread_data[t].working = 1;
        pthread_cond_signal(&thread_data[t].cond);
        pthread_mutex_unlock(&mutex);
    }

    // Joignez tous les threads
    for(int t = 0; t < k; t++) {
        pthread_join(worker_tid[t], NULL);
    }

    // Afficher tous les nombres premiers
    for(int i = 2; i <= n; i++) {
        if(prime[i]) {
            printf("%d ", i);
        }
    }

    printf("\n");

    return 0;
}