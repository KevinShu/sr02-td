#!/opt/homebrew/bin/gnuplot

set term png
set output "plot.png"
set title "Temps moyen en fonction de n"
set xlabel "n"
set ylabel "Temps moyen (secondes)"
set grid

plot "plotdata.txt" using 1:2 with linespoints title "seq", \
     "plotdata.txt" using 1:3 with linespoints title "k=1", \
     "plotdata.txt" using 1:4 with linespoints title "k=2", \
     "plotdata.txt" using 1:5 with linespoints title "k=3", \
     "plotdata.txt" using 1:6 with linespoints title "k=4", \
     "plotdata.txt" using 1:7 with linespoints title "k=5", \
     "plotdata.txt" using 1:8 with linespoints title "k=6", \
     "plotdata.txt" using 1:9 with linespoints title "k=7", \