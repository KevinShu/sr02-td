#define _XOPEN_SOURCE 700
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>

int pid_fils;
int pid_pere;
int i_fils = 0;
int i_pere = 0;
int nb_l = 1;

struct sigaction act_fils, act_pere;

void fonction_fils() {
    for (int i = 0; i < nb_l && 'a'+i_fils <= 'z'; i++){
        printf("%c", 'a' + i_fils++);
        fflush(stdout);
    }
    nb_l++; // nb_l droivent etre incrementer dans fils et pere car il s'agit de deux proessus
    sleep(1);
    kill(pid_pere, SIGUSR1);
}

void fonction_pere() {
    for (int i = 0; i < nb_l && 'A'+i_pere <= 'Z'; i++){
        printf("%c", 'A' + i_pere++);
        fflush(stdout);
    }
    nb_l++; // nb_l droivent etre incrementer dans fils et pere car il s'agit de deux proessus
    sleep(1);
    kill(pid_fils, SIGUSR1);
}

int main(){
    pid_fils = fork();
    if (pid_fils == 0){
        printf("Processus fils, pid = %d\n", getpid());
        pid_fils = getpid();
        pid_pere = getppid();
        act_fils.sa_handler = fonction_fils;
        sigaction(SIGUSR1, &act_fils, 0);
        printf("Processus fils, pid_fils = %d, pid_pere = %d\n",pid_fils, pid_pere);
        while(1){
            pause();
        }

    } else {
        printf("Processus pere, pid = %d\n",getpid());
        pid_pere = getpid();
        act_pere.sa_handler = fonction_pere;
        sigaction(SIGUSR1, &act_pere, 0);
        printf("Processus pere, pid_fils = %d, pid_pere = %d\n",pid_fils, pid_pere);
        sleep(1);
        kill(pid_fils, SIGUSR1); // envoi de tout premier signal de pere a fils
        while(1){
            pause();
        }
    }

}