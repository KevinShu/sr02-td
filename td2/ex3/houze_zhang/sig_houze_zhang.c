#define _XOPEN_SOURCE 700
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <signal.h>

extern void initrec();
extern int attendreclic();
void captpere(int sig);
void captfils(int sig);

struct sigaction fils, pere;
int cnt_pere = 0;
int cnt_fils = 0;
void captpere(int sig){
    if (++cnt_pere == 3){
        exit(0);
    }
}

void captfils(int sig){
    if (++cnt_fils == 3){
        detruitrec();
        exit(0);
    }
    rectvert(5);
}

int main(){
    pid_t pid = fork();
    if (pid == 0){
        // child
        initrec();
        fils.sa_sigaction = captfils;
        sigaction(SIGINT, &fils, NULL);
        sigaction(SIGUSR1, &fils, NULL);
        int i;
        while((i = attendreclic()) != -1){
            if (i == 0){
                kill(getppid(), SIGINT);
            }
            if (i == 1){
                kill(getpid(), SIGINT);
            }
        }
        exit(0);
    }
   else{
        pere.sa_sigaction = captpere;
        sigaction(SIGINT, &pere, NULL);
        while(1) {
            int rest = sleep(10);
            if (rest != 0){
                printf("%ds reste a faire\n", rest);
            }
        }
    }
    exit(0);
}
