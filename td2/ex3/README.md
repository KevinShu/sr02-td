# SR02 TD Ex3

| **Auteurs**             |   **Contact**              |
|:------------------------| :------------- |
| Bingqian Shu            |  bingqian.shu@etu.utc.fr |
| Houze Zhang            |  houze.zhang@etu.utc.fr |
| Zhenyang Xu            |  zhenyang.xu@etu.utc.fr |
| M. Hicham LAKHLEF |  hicham.lakhlef@utc.fr |

## Compilation

```
sudo apt-get install libx11-dev
gcc -c sigx.c -o sigx.o
gcc -o sig sig.c sigx.o -L/usr/X11R6/lib -lX11
```

## Remarques
- L'appel de la fonction `rectvert(5)` dans le handleur de signal `captfils()` bloquerai le processus fils. Pourtant, un appel qui vient du `main()` ne provoque pas tel problème.
- Après la recherche, nous avons identifié que cet appel a été bloqué par l'appel de `XAllocNamedColor()` dans `setbigrec()`.
- Le workaround sera de bypasser l'appel de la fonction `setbigrec()` et de passer directement les valeurs RVB en paramètres dans la fonction `XSetForeground()`.
- La fonction `void rectvert(int n)`modifié est:
```
void rectvert(int n)
{
	XSetForeground (xdisplay, xgc, (0 + (255<<8) + (0<<16)));
	XFillRectangle (xdisplay, xwindow, xgc, 25, 140, 400, 90);
    XFlush (xdisplay);
	sleep(n);
	XSetForeground (xdisplay, xgc, (0 + (0<<8) + (255<<16)));
	XFillRectangle (xdisplay, xwindow, xgc, 25, 140, 400, 90);
    XFlush (xdisplay);
}
```
Cette fonction a été adapté dans le [code de Bingqian SHU](https://gitlab.com/KevinShu/sr02-td/-/tree/main/ex3/bingqian_shu)

- Le [code de Houze ZHANG](https://gitlab.com/KevinShu/sr02-td/-/tree/main/ex3/houze_zhang) et le [code de Zhenyang XU](https://gitlab.com/KevinShu/sr02-td/-/tree/main/ex3/zhenyang_xu) a adapté comme solution d'envoyer un signal `SIGINT` au fils depuis le processus père chaque fois que le père recoit un `SIGINT`, c-à-d:
```
void captpere() {
    sigint_count_pere++;
    printf("Père: SIGINT reçu (%d)\n", sigint_count_pere);
    if (sigint_count_pere >= 3) {
	  kill(pid,SIGINT);
        exit(0);
    }
    kill(pid, SIGINT);
}
```
et de déplacer l'appel `rectvert(5);` dans la boucle `while` du fils dans le `main`.

