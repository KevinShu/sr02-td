#include <stdio.h>
#include <signal.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>

extern void initrec();
extern int attendreclic();
extern void rectvert(int n);
extern void detruitrec();
extern void ecritrec(char *buf, int lng);
extern void setbigrec (char *col);


int sigint_count_pere = 0;
int sigint_count_fils = 0;
pid_t pid;
struct sigaction act_fils, act_pere;
int run_rectvert = 0;

void captpere() {
    sigint_count_pere++;
    printf("Père: SIGINT reçu (%d)\n", sigint_count_pere);
    if (sigint_count_pere >= 3) {
	  kill(pid,SIGINT);
        exit(0);
    }
    kill(pid, SIGINT);
}

void captfils() {
    sigint_count_fils++;
    printf("Fils: SIGINT reçu (%d)\n", sigint_count_fils);
    run_rectvert = 1; // 设置 run_rectvert 为 1
    if (sigint_count_fils >= 3) {
        detruitrec();
        exit(0);
    }
}

int main() {
    int status;
    int n;

    pid = fork();
    if (pid == 0) {
        initrec();
	  act_fils.sa_handler = captfils;
	  sigaction(SIGINT,&act_fils,0);
        int i;
        while ((i = attendreclic()) != -1) {
    	  if (i == 0) {
            kill(getppid(), SIGINT);
    	  }
    	  if (run_rectvert) {
            rectvert(5);
            run_rectvert = 0;
          }
          }
        detruitrec();
        exit(0);
    } else if (pid > 0) {
        act_pere.sa_handler = captpere;
	  sigaction(SIGINT,&act_pere,0);
        while (1) {
            n = sleep(10);
            if (n > 0) {
                printf("Père: Temps restant d'attente: %d s\n", n);
            }
        }
    } else {
        perror("Erreur lors de la création du processus fils");
        exit(1);
    }

    wait(&status);
    return 0;
}
